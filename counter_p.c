#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

int counter = 0;

int main(int argc, char* argv[])
{

	int n;
        #pragma omp parallel for
	for(n=0;n<100;n++)
        {
		//printf("Updating the counter from thread = %d\n", omp_get_thread_num());
		int newcounter = counter + 1;
		int j; for (j=0;j<0xfff;j++);
		counter = newcounter;

        }

	printf ("The counter is %d\n", counter);

        return 0;
}
