#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

int counter = 0;


int main(int argc, char* argv[])
{
	omp_lock_t m;

	omp_init_lock(&m);
	
	int i;

        #pragma omp parallel for
        for(i=0;i<100;i++)
        {
		omp_set_lock(&m);
                //printf("Updating the counter from thread = %d\n", omp_get_thread_num());
                int newcounter = counter + 1;

                int j;for(j=0;j<0xfff;j++);

                counter = newcounter;
		omp_unset_lock(&m);
        }

	printf ("The counter is %d\n", counter);

        return 0;
}
