%: %.c
	gcc $@.c -Wall -o $@ -fopenmp

all: counter_s counter_p counter_mutex counter_semaphore

clean:
	rm -f counter_s counter_p counter_mutex counter_semaphore
