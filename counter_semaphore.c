#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <semaphore.h>


sem_t s; //semphare s to protect the counter

int counter = 0;


int main(int argc, char* argv[])
{
        sem_init(&s, 0, 1); // initialize s semaphre with value 1

	int i;
	#pragma omp parallel for
        for(i=0;i<100;i++)
        {    
                sem_wait(&s);
                //printf("Updating the counter from thread = %d\n", omp_get_thread_num());
                int newcounter = counter + 1;
                
                int j;for(j=0;j<0xfff;j++);
                
                counter = newcounter;
                sem_post(&s);

        }       

	printf ("The counter is %d\n", counter);

        return 0;
}
